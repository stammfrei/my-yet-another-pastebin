module example.com/m/v2

go 1.22.2

require (
	github.com/Azure/go-autorest v14.2.0+incompatible // indirect
	github.com/Azure/go-autorest/autorest/date v0.3.0 // indirect
	golang.org/x/text v0.15.0 // indirect
)
