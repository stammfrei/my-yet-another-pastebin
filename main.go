package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
)

const (
	pasteDir        = "./pastes"
	sizeLimit       = 1024 * 1024 * 1024
	ID_LENGTH_LIMIT = 6
)

type Paste struct {
	Id      string
	Content string
}

type PasteNotFoundError struct {
	Id  string
	Err error
}

func (e *PasteNotFoundError) Error() string {
	return "PasteNotFoundError: paste with id:" + e.Id + "not found, err:" + e.Err.Error()
}

func GetPaste(id string) (paste *Paste, err error) {
	file, err := os.Open(pasteDir + "/" + id)
	if err != nil {
		return nil, err
	}

	defer file.Close()

	content, err := os.ReadFile(file.Name())
	if err != nil {
		return nil, &PasteNotFoundError{Id: id, Err: err}
	}

	return &Paste{Id: id, Content: string(content)}, nil
}

func GeneratePasteId() string {
	const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	b := make([]byte, ID_LENGTH_LIMIT)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func (p *Paste) Save() error {
	err := os.WriteFile(pasteDir+"/"+p.Id, []byte(p.Content), 0644)
	if err != nil {
		return err
	}

	return nil
}

func (p *Paste) Delete() error {
	err := os.Remove(pasteDir + "/" + p.Id)
	if err != nil {
		return err
	}

	return nil
}

func GetPasteFromRequest(request *http.Request) (paste *Paste, err error) {
	file, header, err := request.FormFile("file")
	if err != nil {
		return nil, fmt.Errorf("Failed to get file: %s", err)
	}

	if header.Size > sizeLimit {
		return nil, fmt.Errorf("File too large, limit is %d bytes", sizeLimit)
	}

	buffer := make([]byte, header.Size)
	readSize, err := file.Read(buffer)
	if err != nil {
		return nil, fmt.Errorf("Failed to save paste content")
	} else if readSize != int(header.Size) || readSize > sizeLimit {
		return nil, fmt.Errorf("Bad file size")
	} else if readSize == 0 {
		return nil, fmt.Errorf("No content received")
	}

	return &Paste{Id: GeneratePasteId(), Content: string(buffer)}, nil
}

func PasteGetHandler(responseWriter http.ResponseWriter, request *http.Request) {
	requestId := strings.TrimPrefix(request.URL.Path, "/")
	err := validatePasteId(requestId)
	if err != nil {
		http.Error(responseWriter, err.Error(), http.StatusBadRequest)
		return
	}

	fmt.Println("Request to get paste:", requestId)

	paste, err := GetPaste(requestId)
	if err != nil {
		fmt.Println(err)
		http.Error(responseWriter, fmt.Sprintf("Paste not found: %s", requestId), http.StatusNotFound)
		return
	}

	fmt.Println("Paste found with id:", paste.Id)

	responseWriter.Header().Add("Content-Type", "text/plain")
	responseWriter.Write([]byte(paste.Content))
}

func PastePostHandler(responseWriter http.ResponseWriter, request *http.Request) {
	fmt.Println("Request to save paste from:", request.RemoteAddr)

	paste, err := GetPasteFromRequest(request)
	if err != nil {
		http.Error(responseWriter, err.Error(), http.StatusBadRequest)
		return
	}

	err = paste.Save()
	if err != nil {
		http.Error(responseWriter, fmt.Sprintf("Failed to save paste: %s", paste.Id), http.StatusInternalServerError)
		return
	}

	fmt.Println("Paste saved to id:", paste.Id)

	responseWriter.Header().Add("Content-Type", "text/plain")
	responseWriter.Write([]byte(request.URL.Scheme + request.Host + "/" + paste.Id))
}

func validatePasteId(id string) error {
	if id == "" {
		return fmt.Errorf("Missing paste ID")
	}

	if len(id) > ID_LENGTH_LIMIT || len(id) <= 0 {
		return fmt.Errorf("Invalid paste ID length, must be %d characters or less", ID_LENGTH_LIMIT)
	}

	for _, r := range id {
		if !(r >= 'a' && r <= 'z' || r >= 'A' && r <= 'Z' || r >= '0' && r <= '9') {
			return fmt.Errorf("Invalid character '%c' in paste ID", r)
		}
	}

	return nil
}

func PastePutHandler(responseWriter http.ResponseWriter, request *http.Request) {
	requestId := strings.ToLower(strings.TrimPrefix(request.URL.Path, "/"))

	err := validatePasteId(requestId)
	if err != nil {
		http.Error(responseWriter, err.Error(), http.StatusBadRequest)
		return
	}

	fmt.Println("Request to update paste:", requestId)

	newPaste, err := GetPasteFromRequest(request)
	if err != nil {
		http.Error(responseWriter, err.Error(), http.StatusBadRequest)
		return
	}

	newPaste.Id = requestId

	err = newPaste.Save()
	if err != nil {
		http.Error(responseWriter, fmt.Sprintf("Failed to save paste: %s", newPaste.Id), http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Add("Content-Type", "text/plain")
	responseWriter.Write([]byte(request.URL.Scheme + request.Host + "/" + newPaste.Id))
}

func PasteDeleteHandler(responseWriter http.ResponseWriter, request *http.Request) {
	requestId := strings.TrimPrefix(request.URL.Path, "/")
	if requestId == "" {
		http.Error(responseWriter, fmt.Sprintf("Missing paste ID"), http.StatusNotFound)
		return
	}

	fmt.Println("Request to delete paste:", requestId)

	paste, err := GetPaste(requestId)
	if err != nil {
		http.Error(responseWriter, fmt.Sprintf("Paste not found: %s", requestId), http.StatusNotFound)
		return
	}

	err = paste.Delete()
	if err != nil {
		http.Error(responseWriter, fmt.Sprintf("Failed to delete paste: %s", requestId), http.StatusInternalServerError)
		return
	}

	responseWriter.WriteHeader(http.StatusOK)
}

func PasteHandler(responseWriter http.ResponseWriter, request *http.Request) {
	switch request.Method {
	case "GET":
		PasteGetHandler(responseWriter, request)
	case "POST":
		PastePostHandler(responseWriter, request)
	case "PUT":
		PastePutHandler(responseWriter, request)
	case "DELETE":
		PasteDeleteHandler(responseWriter, request)
	default:
		responseWriter.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func main() {
	err := os.MkdirAll(pasteDir, 0755)
	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/", PasteHandler)
	log.Fatal(http.ListenAndServe(":9999", nil))
}
