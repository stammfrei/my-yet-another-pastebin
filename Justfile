
build:
  go build .

run: build
  go run .

watch +command:
  watchexec -w {{justfile_directory()}} -e go -c -r just {{command}}
